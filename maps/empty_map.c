#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv)
{
	int rows, cols, i, j;
	if (argc != 3)
		return EXIT_FAILURE;
	rows = atoi(argv[1]);
	cols = atoi(argv[2]);
	for (i = 0 ; i < rows ; ++i) {
		for (j = 0 ; j < cols ; ++j)
			printf("0 ");
		printf("\n");
	}
	return EXIT_SUCCESS;
}
