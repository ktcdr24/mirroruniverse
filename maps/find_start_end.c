#include <stdio.h>


int main(void)
{
	int n, p = 1;
	while (scanf("%d", &n) == 1) {
		if (n == 2)
			printf("2: %d\n", p);
		else if (n == 3)
			printf("3: %d\n", p);
		p++;
	}
	return 0;
}
