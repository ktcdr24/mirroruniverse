#include <queue>
#include <cstdio>
#include <cstdlib>
#include <ctime>

using namespace std;


struct point {

	int i, j;

	point(int _i, int _j):
		i(_i),
		j(_j) {}
};

int main(int argc, char **argv)
{
	if (argc != 4) {
		arg_error:
		fprintf(stderr, "%s: Wrong arguments\n", argv[0]);
		return EXIT_FAILURE;
	}
	srand(time(0));
	int rows = atoi(argv[1]);
	int cols = atoi(argv[2]);
	double perc = atof(argv[3]);
	if (rows <= 0 || cols <= 0 || perc < 0.0 || perc >= 1.0)
		goto arg_error;
	char **map = (char**) malloc(rows * sizeof(char*));
	for (int i = 0 ; i != rows ; ++i)
		map[i] = (char*) malloc(cols * sizeof(char));
	int whites;
	int flooded;
	int start;
	int end;
	int tries = 0;
	do {
		tries++;
		whites = 0;
		flooded = -1;
		point s(0, 0);
		for (int i = 0 ; i != rows ; ++i) {
			for (int j = 0 ; j != rows ; ++j) {
				if (rand() / (double) RAND_MAX < perc)
					map[i][j] = 2;
				else {
					map[i][j] = 0;
					s.i = i;
					s.j = j;
					whites++;
				}
			}
		}
		if (whites < 2) continue;
		start = rand() % whites;
		do {
			end = rand() % whites;
		} while (end == start);
		queue <point> flood_queue;
		flood_queue.push(s);
		flooded = 1;
		map[s.i][s.j] = 1;
		do {
			point p = flood_queue.front();
			flood_queue.pop();
			for (int di = -1 ; di <= 1 ; ++di)
				for (int dj = -1 ; dj <= 1 ; ++dj) {
					int i = p.i + di;
					int j = p.j + dj;
					if (i < 0 || i >= rows || j < 0 || j >= cols || (di | dj) == 0)
						continue;
					if (map[i][j] == 0) {
						map[i][j] = 1;
						if (flooded == start)
							map[i][j] = 4;
						else if (flooded == end)
							map[i][j] = 3;
						flooded++;
						flood_queue.push(point(i, j));
					}
				}
		} while (!flood_queue.empty());
	} while (flooded != whites);
	for (int i = 0 ; i != rows ; ++i) {
		for (int j = 0 ; j != cols ; ++j)
			printf("%d ", map[i][j]-1);
		printf("\n");
		free(map[i]);
	}
	free(map);
	fprintf(stderr, "Tries: %d\n", tries);
	return EXIT_SUCCESS;
}

